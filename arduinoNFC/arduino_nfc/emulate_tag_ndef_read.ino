
#include "SPI.h"
#include "NfcAdapter.h"
#include "PN532_SPI.h"
#include "emulatetag.h"
#include "NdefMessage.h"

PN532_SPI pn532spi(SPI, 10);
EmulateTag nfc(pn532spi);

NfcAdapter lector = NfcAdapter(pn532spi);

uint8_t ndefBuf[120];
NdefMessage message;
int messageSize;

uint8_t uid[3] = { 0x12, 0x34, 0x56 };

void setup()
{
  Serial.begin(115200);
  Serial.println("------- Emulate Tag --------");
  
  message = NdefMessage();
  message.addUriRecord("http://www.seeedstudio.com");
  messageSize = message.getEncodedSize();
  if (messageSize > sizeof(ndefBuf)) {
      Serial.println("ndefBuf is too small");
      while (1) { }
  }
  
  Serial.print("Ndef encoded message size: ");
  Serial.println(messageSize);

  message.encode(ndefBuf);
  
  // comment out this command for no ndef message
  //nfc.setNdefFile(ndefBuf, messageSize);
  
  // uid must be 3 bytes!
  nfc.setUid(uid);
  
  nfc.init();
}
void formatear(NfcAdapter& lector){
  Serial.println("grabar secuencia de grabado");
    NdefMessage message = NdefMessage();
    message.addMimeMediaRecord("text/plain","holakace");
    bool grabado = lector.write(message);
  if (grabado) {
    Serial.println("Success. Try reading this tag with your phone.");        
  } else {
    Serial.println("Write failed.");
  }
}
void loop(){
    boolean success;
     uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;
    // uncomment for overriding ndef in case a write to this tag occured
    //nfc.setNdefFile(ndefBuf, messageSize); 
    
    // start emulation (blocks)
   // nfc.emulate();
    // or start emulation with timeout
    if(!nfc.emulate(1000)){ // timeout 1 second

    }
    
    // deny writing to the tag
    // nfc.setTagWriteable(false);
    
    if(nfc.writeOccured()){

       uint8_t* tag_buf;
       uint16_t length;
       
       nfc.getContent(&tag_buf, &length);
       NdefMessage msg = NdefMessage(tag_buf, length);
       int payloadLength = msg.getRecord(0).getPayloadLength();
       uint8_t data[7];
       msg.getRecord(0).getPayload(data);
       PrintHexChar(data,payloadLength);
    }
      lector.begin();
      if(lector.tagPresent()){
        NfcTag tag = lector.read();
        if (tag.hasNdefMessage()){
           NdefMessage message = tag.getNdefMessage();
           Serial.print("UID: ");Serial.println(tag.getUidString());
           NdefRecord record = message.getRecord(0);
           int payloadLength = record.getPayloadLength();
           byte payload[payloadLength];
           record.getPayload(payload);
           PrintHexChar(payload, payloadLength);
        } else {
          formatear(lector);
        }
      }
       
      
      //volvemos a cambiar la configuracion
      nfc.setUid(uid);
      nfc.init();
    

    delay(1000);
}
